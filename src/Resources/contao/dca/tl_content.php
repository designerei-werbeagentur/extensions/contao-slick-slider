<?php

$GLOBALS['TL_DCA']['tl_content']['palettes']['slickSlider'] =
    '{type_legend},type;'
    . '{slider_legend},sliderSRC,size,fullsize;'
    . '{template_legend:hide},customTpl;'
    . '{protected_legend:hide},protected;'
    . '{expert_legend:hide},guests,cssID;'
    . '{invisible_legend:hide},invisible,start,stop'
;

$GLOBALS['TL_DCA']['tl_content']['palettes']['slickSliderStart'] =
    '{type_legend},type;'
    . '{template_legend:hide},customTpl;'
    . '{protected_legend:hide},protected;'
    . '{expert_legend:hide},guests,cssID;'
    . '{invisible_legend:hide},invisible,start,stop'
;

$GLOBALS['TL_DCA']['tl_content']['palettes']['slickSliderStop'] =
    '{type_legend},type;'
    . '{template_legend:hide},customTpl;'
    . '{protected_legend:hide},protected;'
    . '{expert_legend:hide},guests;'
    . '{invisible_legend:hide},invisible,start,stop'
;

$GLOBALS['TL_DCA']['tl_content']['fields']['sliderSRC'] = [
    'exclude' => true,
    'inputType' => 'fileTree',
    'eval' => array('multiple'=>true, 'mandatory'=>true, 'extensions'=>'jpg,jpeg', 'fieldType'=>'checkbox', 'orderField'=>'orderSRC', 'files'=>true),
    'sql' => "blob NULL",
];

$GLOBALS['TL_DCA']['tl_content']['config']['onload_callback'][] = function ($dc) {
  $objCte = Contao\ContentModel::findByPk($dc->id);
  if ($objCte->type === 'slickSlider' || $objCte->type === 'slickSliderStart' || $objCte->type === 'slickSliderStop') {
    \Contao\Message::addInfo(sprintf($GLOBALS['TL_LANG']['tl_content']['includeTemplate'], 'j_slickSlider'));
  }
};
