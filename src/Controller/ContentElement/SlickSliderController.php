<?php

namespace designerei\ContaoSlickSliderBundle\Controller\ContentElement;

use Contao\ContentModel;
use Contao\CoreBundle\Controller\ContentElement\AbstractContentElementController;
use Contao\CoreBundle\ServiceAnnotation\ContentElement;
use Contao\Template;
use Contao\StringUtil;
use Contao\FilesModel;
use Contao\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @ContentElement("slickSlider",
 *   category="Slick-Slider"
 * )
 */
class SlickSliderController extends AbstractContentElementController
{
    protected function getResponse(Template $template, ContentModel $model, Request $request): ?Response
    {

        // deserialize images
        $objFiles = $model->orderSRC;
        $objFiles = StringUtil::deserialize($objFiles);

        // lightbox id
        $strLightBoxId = 'lb' . $template->id;

        // loop through images and output as picture element
        foreach($objFiles as $key => $objFile)
        {
            $objFile = FilesModel::findByUuid($objFile);

            $picture[$key] = new \stdClass();

            Controller::addImageToTemplate($picture[$key], [
                'singleSRC' => $objFile->path,
                'size' => $template->size,
                'fullsize' => $template->fullsize
            ], null, $strLightBoxId, $objFile);
        }

        // output pictures array to template
        $template->pictures = $picture;

        return $template->getResponse();
    }
}
